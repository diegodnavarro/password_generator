from django.shortcuts import render
import random

# Create your views here.

def home(request):
    return render(request, 'generator/home.html')

def password(request):
    thepass = ""
    length = int(request.GET.get('length',12))
    characters = list('abcdefghijklmnopqrstuvwxyz')
    count = 0

    if request.GET.get('uppercase'):
        characters.extend(list('ABCDEFGHIJKLMNOPQRSTUVWXYZ'))
    if request.GET.get('special'):
        characters.extend(list('!"$%&'))
    if request.GET.get('numbers'):
        characters.extend(list('1234567890'))

    for _ in range(length):
        thepass += random.choice(characters)
        count += 1
        if count == 4:
            thepass += '-'
            count = 0

    if thepass[-1] == '-':
        thepass = thepass[:-1]

    return render(request, 'generator/password.html', {'password': thepass})

def about(request):
    return render(request, 'about/about.html')